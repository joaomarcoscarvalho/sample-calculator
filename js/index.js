function Calculator(){
  this.expression = document.getElementById("expression")
  this.result = document.getElementById("result")

  this.start = () => {
    this.buttons()
  }

  this.buttons = () => {
    document.addEventListener("click", (e) => {
      let el = e.target
      if(el.classList.contains("btn")){
        this.buttonsToDisplay(el.innerText)
      }

      if(el.classList.contains("equal")){
        this.equal()
      }

      if(el.classList.contains("clear")){
        this.clear()
      }
    })
  }

  this.buttonsToDisplay = (value) => {
    this.expression.innerText += value
  }

  this.equal = () => {
    let dataToCalc = this.expression.innerText

    try {
      this.result.innerText = eval(dataToCalc)
    } catch {
      console.error("Operação Inválida")
    }
  }

  this.clear = () => {
    this.expression.innerText = ''
    this.result.innerText = ''
  }
}


let calculator = new Calculator()
calculator.start()